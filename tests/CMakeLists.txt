project(qtolm-tests VERSION ${PROJECT_VERSION} LANGUAGES CXX)

find_package(Qt5 COMPONENTS Core REQUIRED)

set(SOURCE_FILES
    main.cpp
)

set(INCLUDE_DIRS
    .
)

set(ALL_FILES ${HEADERS_FILES} ${SOURCE_FILES})

add_executable(${PROJECT_NAME} ${ALL_FILES})

target_link_libraries(${PROJECT_NAME} Qt5::Core QtOlm)

target_include_directories(${PROJECT_NAME} PRIVATE ${INCLUDE_DIRS})
