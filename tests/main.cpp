#include "account.h"
#include "errors.h"
#include "groupsession.h"
#include "message.h"
#include "pk.h"
#include "session.h"
#include "utils.h"

#include <QObject>
#include <QtDebug>

class AccountTest : public QObject {
 public:
  explicit AccountTest(QObject* parent = nullptr) : QObject(parent) {}

  void testCreation();
  void testPickle();
  void testInvalidUnpickle();
  void testPassphrasePickle();
  void testWrongPassphrasePickle();
  void testOneTimeKeys();
  void testValidSignature();
  void testInvalidSignature();
};

void AccountTest::testCreation() {
  QtOlm::Account* alice = new QtOlm::Account();
  Q_ASSERT(!alice->curve25519IdentityKey().isEmpty() &&
           !alice->ed25519IdentityKey().isEmpty());
}

void AccountTest::testPickle() {
  QtOlm::Account* alice = new QtOlm::Account();
  QByteArray pickle = alice->pickle();
  Q_ASSERT(alice->identityKeys() == QtOlm::Account(pickle).identityKeys());
}

void AccountTest::testInvalidUnpickle() {
  try {
    QtOlm::Account* alice = new QtOlm::Account(QByteArray());
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void AccountTest::testPassphrasePickle() {
  QtOlm::Account* alice = new QtOlm::Account();
  QString passphrase("It's a secret to everybody");
  QByteArray pickle = alice->pickle(passphrase);
  Q_ASSERT(alice->identityKeys() ==
           QtOlm::Account(pickle, passphrase).identityKeys());
}

void AccountTest::testWrongPassphrasePickle() {
  QtOlm::Account* alice = new QtOlm::Account();
  QString passphrase("It's a secret to everybody");
  QByteArray pickle = alice->pickle(passphrase);
  try {
    QtOlm::Account* alice2 = new QtOlm::Account(pickle);
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::AccountKeyError*>(e));
  }
}

void AccountTest::testOneTimeKeys() {
  QtOlm::Account* alice = new QtOlm::Account();
  alice->generateOneTimeKeys(10);
  Q_ASSERT(!alice->oneTimeKeys().isEmpty());
  Q_ASSERT(alice->curve25519OneTimeKeys().size() == 10);
  alice->markKeysAsPublished();
  Q_ASSERT(alice->curve25519OneTimeKeys().isEmpty());
}

const QString message = "This is a message.";

void AccountTest::testValidSignature() {
  QtOlm::Account* alice = new QtOlm::Account();
  QByteArray signature = alice->sign(message);
  QByteArray signingKey = alice->ed25519IdentityKey();

  Q_ASSERT(!signature.isEmpty());
  Q_ASSERT(!signingKey.isEmpty());

  QtOlm::ed25519Verify(signingKey, message, signature);
}

void AccountTest::testInvalidSignature() {
  QtOlm::Account* alice = new QtOlm::Account();
  QtOlm::Account* bob = new QtOlm::Account();
  QByteArray signature = alice->sign(message);
  QByteArray signingKey = bob->ed25519IdentityKey();

  Q_ASSERT(!signature.isEmpty());
  Q_ASSERT(!signingKey.isEmpty());

  try {
    QtOlm::ed25519Verify(signingKey, message, signature);
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::SignatureError*>(e));
  }
}

class SessionTest : public QObject {
 public:
  explicit SessionTest(QObject* parent = nullptr) : QObject(parent) {}

  void testCreate();
  void testPickle();
  void testInvalidPickle();
  void testWrongPassphrasePickle();
  void testEncrypt();
  void testEmptyMessage();
  void testInboundWithId();
  void testTwoMessages();
  void testMatches();
  void testInvalid();
  void testDoesntMatch();
};

std::tuple<QtOlm::Account*, QtOlm::Account*, QtOlm::Session*> createSession() {
  QtOlm::Account* alice = new QtOlm::Account();
  QtOlm::Account* bob = new QtOlm::Account();
  bob->generateOneTimeKeys(1);
  QByteArray idKey = bob->curve25519IdentityKey();
  QByteArray oneTime = bob->curve25519OneTimeKeys().values()[0].toByteArray();
  QtOlm::OutboundSession* session =
      new QtOlm::OutboundSession(alice, idKey, oneTime);
  return std::tuple<QtOlm::Account*, QtOlm::Account*, QtOlm::Session*>(
      alice, bob, session);
}

void SessionTest::testCreate() {
  QtOlm::Session* session1 = std::get<2>(createSession());
  QtOlm::Session* session2 = std::get<2>(createSession());
  Q_ASSERT(session1);
  Q_ASSERT(session2);
  Q_ASSERT(session1->id() != session2->id());
}

void SessionTest::testPickle() {
  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();
  Q_ASSERT(QtOlm::Session(session->pickle()).id() == session->id());
}

void SessionTest::testInvalidPickle() {
  try {
    QtOlm::Session("", "");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void SessionTest::testWrongPassphrasePickle() {
  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();
  QString passphrase("It's a secret to everybody");
  QByteArray pickle = alice->pickle(passphrase);
  try {
    new QtOlm::Session(pickle);
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::AccountKeyError*>(e));
  }
}

void SessionTest::testEncrypt() {
  QString plainText = "It's a secret to everybody";

  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();

  QtOlm::_Message* message = session->encrypt(plainText);
  Q_ASSERT(dynamic_cast<QtOlm::PreKeyMessage*>(message));
  QtOlm::PreKeyMessage* preKeyMessage =
      dynamic_cast<QtOlm::PreKeyMessage*>(message);

  QtOlm::InboundSession* bobSession =
      new QtOlm::InboundSession(bob, preKeyMessage);
  Q_ASSERT(plainText == bobSession->decrypt(message));
}

void SessionTest::testEmptyMessage() {
  try {
    QtOlm::PreKeyMessage("");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }

  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();
  QtOlm::PreKeyMessage* empty = new QtOlm::PreKeyMessage("x");

  try {
    session->decrypt(empty);
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::Base64Error*>(e));
  }
}

void SessionTest::testInboundWithId() {
  QString plainText = "It's a secret to everybody";

  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();

  QtOlm::_Message* message = session->encrypt(plainText);
  QByteArray aliceId = alice->curve25519IdentityKey();
  QtOlm::Session* bobSession = new QtOlm::InboundSession(
      bob, dynamic_cast<QtOlm::PreKeyMessage*>(message), aliceId);

  Q_ASSERT(plainText == bobSession->decrypt(message));
}

void SessionTest::testTwoMessages() {
  QString plainText = "It's a secret to everybody";

  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();

  QtOlm::_Message* message = session->encrypt(plainText);
  QByteArray aliceId = alice->curve25519IdentityKey();
  QtOlm::Session* bobSession = new QtOlm::InboundSession(
      bob, dynamic_cast<QtOlm::PreKeyMessage*>(message), aliceId);
  bob->removeOneTimeKeys(bobSession);

  Q_ASSERT(plainText == bobSession->decrypt(message));

  QString bobPlainText = "Grumble, Grumble";
  QtOlm::_Message* bobMessage = bobSession->encrypt(bobPlainText);

  Q_ASSERT(dynamic_cast<QtOlm::Message*>(bobMessage));
  Q_ASSERT(bobPlainText == session->decrypt(bobMessage));
}

void SessionTest::testMatches() {
  QString plainText = "It's a secret to everybody";

  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();

  QtOlm::_Message* message = session->encrypt(plainText);
  QByteArray aliceId = alice->curve25519IdentityKey();
  QtOlm::Session* bobSession = new QtOlm::InboundSession(
      bob, dynamic_cast<QtOlm::PreKeyMessage*>(message), aliceId);

  Q_ASSERT(plainText == bobSession->decrypt(message));

  QtOlm::_Message* message2 = session->encrypt("Hey! Listen!");

  Q_ASSERT(bobSession->matches(dynamic_cast<QtOlm::PreKeyMessage*>(message2)));
  Q_ASSERT(bobSession->matches(dynamic_cast<QtOlm::PreKeyMessage*>(message2),
                               aliceId));
}

void SessionTest::testInvalid() {
  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();

  QtOlm::PreKeyMessage* message = new QtOlm::PreKeyMessage("x");

  try {
    session->matches(message);
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::Base64Error*>(e));
  }
  try {
    new QtOlm::InboundSession(bob, message);
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::Base64Error*>(e));
  }
  try {
    new QtOlm::OutboundSession(alice, "", "x");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
  try {
    new QtOlm::OutboundSession(alice, "x", "");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void SessionTest::testDoesntMatch() {
  QString plainText = "It's a secret to everybody";

  QtOlm::Account *alice, *bob;
  QtOlm::Session* session;
  std::tie(alice, bob, session) = createSession();

  QtOlm::_Message* message = session->encrypt(plainText);
  QByteArray aliceId = alice->curve25519IdentityKey();

  QtOlm::Session* bobSession = new QtOlm::InboundSession(
      bob, dynamic_cast<QtOlm::PreKeyMessage*>(message), aliceId);

  QtOlm::Session* newSession = std::get<2>(createSession());

  QtOlm::_Message* newMessage = newSession->encrypt(plainText);
  Q_ASSERT(
      !bobSession->matches(dynamic_cast<QtOlm::PreKeyMessage*>(newMessage)));
}

class GroupSessionTest : public QObject {
 public:
  explicit GroupSessionTest(QObject* parent = nullptr) : QObject(parent) {}

  void testCreate();
  void testSessionID();
  void testIndex();
  void testOutboundPickle();
  void testInvalidUnpickle();
  void testInboundCreate();
  void testInvalidDecrypt();
  void testInboundPickle();
  void testInboundExport();
  void testFirstIndex();
  void testEncrypt();
  void testDecrypt();
  void testDecryptTwice();
  void testDecryptFailure();
  void testID();
  void testInboundFail();
  void testOutboundPickleFail();
};

void GroupSessionTest::testCreate() {
  new QtOlm::OutboundGroupSession();
}

void GroupSessionTest::testSessionID() {
  QtOlm::OutboundGroupSession* session = new QtOlm::OutboundGroupSession();
  session->id();
}

void GroupSessionTest::testIndex() {
  QtOlm::OutboundGroupSession* session = new QtOlm::OutboundGroupSession();
  Q_ASSERT(session->messageIndex() == 0);
}

void GroupSessionTest::testOutboundPickle() {
  QtOlm::OutboundGroupSession* session = new QtOlm::OutboundGroupSession();
  QByteArray pickle = session->pickle();
  Q_ASSERT(session->id() == QtOlm::OutboundGroupSession(pickle).id());
}

void GroupSessionTest::testInvalidUnpickle() {
  try {
    new QtOlm::OutboundGroupSession(QByteArray());
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }

  try {
    new QtOlm::InboundGroupSession(QByteArray());
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void GroupSessionTest::testInboundCreate() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  new QtOlm::InboundGroupSession(outbound->sessionKey());
}

void GroupSessionTest::testInvalidDecrypt() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());

  try {
    inbound->decrypt("");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void GroupSessionTest::testInboundPickle() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  QByteArray pickle = inbound->pickle();
  new QtOlm::InboundGroupSession(pickle);
}

void GroupSessionTest::testInboundExport() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  QtOlm::InboundGroupSession* imported = new QtOlm::InboundGroupSession(
      inbound->exportSession(inbound->firstKnownIndex()),
      QtOlm::InboundGroupSession::Initialization::Import);
  QString result;
  int index;
  std::tie(result, index) = (imported->decrypt(outbound->encrypt("Test")));
  Q_ASSERT(result == "Test");
  Q_ASSERT(index == 0);
}

void GroupSessionTest::testFirstIndex() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  int index = inbound->firstKnownIndex();
}

void GroupSessionTest::testEncrypt() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  int index;
  QString decryptedText;
  std::tie(decryptedText, index) = inbound->decrypt(outbound->encrypt("Test"));
  Q_ASSERT(decryptedText == "Test");
  Q_ASSERT(index == 0);
}

void GroupSessionTest::testDecrypt() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  int index;
  QString decryptedText;
  std::tie(decryptedText, index) = inbound->decrypt(outbound->encrypt("Test"));
  Q_ASSERT(decryptedText == "Test");
  Q_ASSERT(index == 0);
}

void GroupSessionTest::testDecryptTwice() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  outbound->encrypt("Test 1");
  int index;
  QString decryptedText;
  std::tie(decryptedText, index) =
      inbound->decrypt(outbound->encrypt("Test 2"));
  Q_ASSERT(decryptedText == "Test 2");
  Q_ASSERT(index == 1);
}

void GroupSessionTest::testDecryptFailure() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  QtOlm::OutboundGroupSession* eveOutbound = new QtOlm::OutboundGroupSession();
  try {
    inbound->decrypt(eveOutbound->encrypt("Test"));
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::OlmError*>(e));
  }
}

void GroupSessionTest::testID() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QtOlm::InboundGroupSession* inbound =
      new QtOlm::InboundGroupSession(outbound->sessionKey());
  Q_ASSERT(outbound->id() == inbound->id());
}

void GroupSessionTest::testInboundFail() {
  try {
    new QtOlm::InboundGroupSession("");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void GroupSessionTest::testOutboundPickleFail() {
  QtOlm::OutboundGroupSession* outbound = new QtOlm::OutboundGroupSession();
  QByteArray pickle = outbound->pickle("Test");

  try {
    new QtOlm::OutboundGroupSession(pickle, "");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::AccountKeyError*>(e));
  }
}

class PkTest : public QObject {
 public:
  explicit PkTest(QObject* parent = nullptr) : QObject(parent) {}

  void testInvalidEncryption();
  void testDecryption();
  void testInvalidDecryption();
  void testPickling();
  void testInvalidUnpickling();
  void testInvalidPassPickling();
};

void PkTest::testInvalidEncryption() {
  try {
    new QtOlm::PkEncryption("");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void PkTest::testDecryption() {
  QtOlm::PkDecryption decryption;
  QtOlm::PkEncryption encryption(decryption.publicKey());
  QString plainText = "It's a secret to everybody.";
  auto message = encryption.encrypt(plainText);
  auto decryptedPlainText = decryption.decrypt(message);
  Q_ASSERT(plainText == decryptedPlainText);
}

void PkTest::testInvalidDecryption() {
  QtOlm::PkDecryption decryption;
  QtOlm::PkEncryption encryption(decryption.publicKey());
  QString plainText = "It's a secret to everybody.";
  auto message = encryption.encrypt(plainText);
  QtOlm::PkMessage fakeMessage(QByteArray("?"), message->mac(),
                               message->cipherText(), message->parent());
  try {
    decryption.decrypt(&fakeMessage);
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::OlmError*>(e));
  }
}

void PkTest::testPickling() {
  QtOlm::PkDecryption decryption;
  QtOlm::PkEncryption encryption(decryption.publicKey());
  QString plainText = "It's a secret to everybody.";
  auto message = encryption.encrypt(plainText);

  QByteArray pickle = decryption.pickle();
  QtOlm::PkDecryption unpickled(pickle, "");
  auto decryptedPlainText = unpickled.decrypt(message);
  Q_ASSERT(plainText == decryptedPlainText);
}

void PkTest::testInvalidUnpickling() {
  try {
    new QtOlm::PkDecryption("", "");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::InvalidArgument*>(e));
  }
}

void PkTest::testInvalidPassPickling() {
  QtOlm::PkDecryption decryption;
  QtOlm::PkEncryption encryption(decryption.publicKey());
  QString plainText = "It's a secret to everybody.";
  auto message = encryption.encrypt(plainText);

  QByteArray pickle = decryption.pickle("Secret");
  try {
    QtOlm::PkDecryption unpickled(pickle, "");
    throw new QtOlm::UnknownError();
  } catch (std::exception* e) {
    Q_ASSERT(dynamic_cast<QtOlm::OlmError*>(e));
  }
}

int main() {
  AccountTest* accountTest = new AccountTest();
  accountTest->testCreation();
  accountTest->testPickle();
  accountTest->testInvalidUnpickle();
  accountTest->testPassphrasePickle();
  accountTest->testWrongPassphrasePickle();
  accountTest->testOneTimeKeys();
  accountTest->testValidSignature();
  accountTest->testInvalidSignature();

  SessionTest* sessionTest = new SessionTest();
  sessionTest->testCreate();
  sessionTest->testPickle();
  sessionTest->testInvalidPickle();
  sessionTest->testWrongPassphrasePickle();
  sessionTest->testEncrypt();
  sessionTest->testEmptyMessage();
  sessionTest->testInboundWithId();
  sessionTest->testTwoMessages();
  sessionTest->testMatches();
  sessionTest->testInvalid();
  sessionTest->testDoesntMatch();

  GroupSessionTest* groupSessionTest = new GroupSessionTest();
  groupSessionTest->testCreate();
  groupSessionTest->testSessionID();
  groupSessionTest->testIndex();
  groupSessionTest->testOutboundPickle();
  groupSessionTest->testInvalidUnpickle();
  groupSessionTest->testInboundCreate();
  groupSessionTest->testInboundExport();
  groupSessionTest->testFirstIndex();
  groupSessionTest->testEncrypt();
  groupSessionTest->testDecrypt();
  groupSessionTest->testDecryptTwice();
  groupSessionTest->testDecryptFailure();
  groupSessionTest->testID();
  groupSessionTest->testInboundFail();
  groupSessionTest->testOutboundPickleFail();

  PkTest* pkTest = new PkTest();
  pkTest->testInvalidEncryption();
  pkTest->testDecryption();
  pkTest->testInvalidDecryption();
  pkTest->testPickling();
  pkTest->testInvalidUnpickling();
  pkTest->testInvalidPassPickling();

  return 0;
}
