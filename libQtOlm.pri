QT += core network
CONFIG += c++14 object_parallel_to_source

SRCPATH = $$PWD/lib
INCLUDEPATH += $$SRCPATH

mac {
    INCLUDEPATH += /usr/local/include
    LIBS += -L/usr/local/lib
}

unix|win32: LIBS += -lolm

HEADERS += \
    $$SRCPATH/account.h \
    $$SRCPATH/utils.h \
    $$SRCPATH/session.h \
    $$SRCPATH/message.h \
    $$SRCPATH/errors.h \
    $$SRCPATH/groupsession.h \
    $$SRCPATH/pk.h

SOURCES += \
    $$SRCPATH/account.cpp \
    $$SRCPATH/session.cpp \
    $$SRCPATH/message.cpp \
    $$SRCPATH/groupsession.cpp \
    $$SRCPATH/pk.cpp
