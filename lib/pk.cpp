#include "pk.h"

#include "utils.h"

using namespace QtOlm;

PkMessage::PkMessage(QByteArray ephermalKey, QByteArray mac, QByteArray cipherText, QObject *parent)
    : QObject(parent)
    , _ephermalKey(ephermalKey)
    , _mac(mac)
    , _cipherText(cipherText)
{
}

PkEncryption::PkEncryption(QByteArray recipientKey, QObject *parent)
    : QObject(parent)
    , _pkEncryption(newPkEncryption())
{
    if (recipientKey.isEmpty())
        throw new InvalidArgument("Recipient key is empty");
    olm_pk_encryption_set_recipient_key(_pkEncryption, recipientKey, recipientKey.length());
}

PkEncryption::~PkEncryption()
{
    olm_clear_pk_encryption(_pkEncryption);
}

PkMessage *PkEncryption::encrypt(QString plainText)
{
    std::string plainTxt = plainText.toStdString();
    size_t encryptLength = olm_pk_encrypt_random_length(_pkEncryption);
    QByteArray randomBuffer = getRandomData(encryptLength);
    size_t cipherTextLength = olm_pk_ciphertext_length(_pkEncryption, plainTxt.length());
    QByteArray cipherText(cipherTextLength, '0');
    size_t macLength = olm_pk_mac_length(_pkEncryption);
    QByteArray mac(macLength, '0');
    size_t ephermalKeySize = olm_pk_key_length();
    QByteArray ephermalKey(ephermalKeySize, '0');

    checkErr(olm_pk_encrypt(_pkEncryption, plainTxt.data(), plainTxt.length(), cipherText.data(), cipherTextLength, mac.data(), macLength, ephermalKey.data(), ephermalKeySize, randomBuffer.data(), encryptLength));

    return new PkMessage(ephermalKey, mac, cipherText);
}

OlmPkEncryption *PkEncryption::newPkEncryption()
{
    return olm_pk_encryption(new uint8_t[olm_pk_encryption_size()]);
}

void PkEncryption::checkErr(size_t code)
{
    if (code != olm_error())
        return;
    std::string lastError = olm_pk_encryption_last_error(_pkEncryption);

    if (lastError == "SUCCESS")
        return;
    if (lastError == "NOT_ENOUGH_RANDOM")
        throw new EntropyError(lastError);
    if (lastError == "OUTPUT_BUFFER_TOO_SMALL" || lastError == "OLM_INPUT_BUFFER_TOO_SMALL")
        throw new BufferError(lastError);
    if (lastError == "BAD_MESSAGE_VERSION" || lastError == "BAD_MESSAGE_FORMAT" || lastError == "BAD_MESSAGE_MAC" || lastError == "BAD_MESSAGE_KEY_ID" || lastError == "UNKNOWN_MESSAGE_INDEX")
        throw new MessageError(lastError);
    if (lastError == "INVALID_BASE64")
        throw new Base64Error(lastError);
    if (lastError == "BAD_ACCOUNT_KEY")
        throw new AccountKeyError(lastError);
    if (lastError == "UNKNOWN_PICKLE_VERSION" || lastError == "CORRUPTED_PICKLE" || lastError == "BAD_LEGACY_ACCOUNT_PICKLE")
        throw new PickleError(lastError);
    if (lastError == "BAD_SESSION_KEY")
        throw new SessionKeyError(lastError);
    if (lastError == "BAD_SIGNATURE")
        throw new SignatureError(lastError);
    throw new OlmError(lastError);
}

PkDecryption::PkDecryption(QObject *parent)
    : QObject(parent)
    , _pkDecryption(newPkDecryption())
{
    size_t randomLength = olm_pk_generate_key_random_length();
    QByteArray randomBuffer = getRandomData(randomLength);

    size_t keyLength = olm_pk_key_length();
    QByteArray keyBuffer(keyLength, '0');

    checkErr(olm_pk_generate_key(_pkDecryption, keyBuffer.data(), keyLength, randomBuffer.data(), randomLength));

    _publicKey = keyBuffer;
}

PkDecryption::PkDecryption(QByteArray pickle, QString passphrase, QObject *parent)
    : QObject(parent)
    , _pkDecryption(newPkDecryption())
{
    if (pickle.isEmpty())
        throw new InvalidArgument("Pickle is empty");

    std::string pass = passphrase.toStdString();
    size_t pubKeyLength = olm_pk_key_length();
    QByteArray pubKeyBuffer(pubKeyLength, '0');

    checkErr(olm_unpickle_pk_decryption(_pkDecryption, pass.data(), pass.length(), pickle.data(), pickle.length(), pubKeyBuffer.data(), pubKeyLength));

    _publicKey = pubKeyBuffer;
}

PkDecryption::~PkDecryption()
{
    olm_clear_pk_decryption(_pkDecryption);
}

QByteArray PkDecryption::pickle(QString passphrase)
{
    std::string pass = passphrase.toStdString();
    size_t pickleLength = olm_pickle_pk_decryption_length(_pkDecryption);
    QByteArray pickleBuffer(pickleLength, '0');
    checkErr(olm_pickle_pk_decryption(_pkDecryption, pass.data(), pass.length(), pickleBuffer.data(), pickleLength));

    return pickleBuffer;
}

QString PkDecryption::decrypt(PkMessage *message)
{
    QByteArray ephermalKey = message->ephermalKey();

    QByteArray mac = message->mac();

    QByteArray cipherText = message->cipherText();

    size_t maxPlaintextLength = olm_pk_max_plaintext_length(_pkDecryption, cipherText.length());

    QByteArray plainTextBuffer(maxPlaintextLength, '0');

    size_t plainTextLength = olm_pk_decrypt(_pkDecryption, ephermalKey.data(), ephermalKey.length(), mac.data(), mac.length(), cipherText.data(), cipherText.length(), plainTextBuffer.data(), maxPlaintextLength);
    checkErr(plainTextLength);

    plainTextBuffer.truncate(plainTextLength);

    return QString(plainTextBuffer);
}

OlmPkDecryption *PkDecryption::newPkDecryption()
{
    return olm_pk_decryption(new uint8_t[olm_pk_decryption_size()]);
}

void PkDecryption::checkErr(size_t code)
{
    if (code != olm_error())
        return;
    std::string lastError = olm_pk_decryption_last_error(_pkDecryption);

    if (lastError == "SUCCESS")
        return;
    if (lastError == "NOT_ENOUGH_RANDOM")
        throw new EntropyError(lastError);
    if (lastError == "OUTPUT_BUFFER_TOO_SMALL" || lastError == "OLM_INPUT_BUFFER_TOO_SMALL")
        throw new BufferError(lastError);
    if (lastError == "BAD_MESSAGE_VERSION" || lastError == "BAD_MESSAGE_FORMAT" || lastError == "BAD_MESSAGE_MAC" || lastError == "BAD_MESSAGE_KEY_ID" || lastError == "UNKNOWN_MESSAGE_INDEX")
        throw new MessageError(lastError);
    if (lastError == "INVALID_BASE64")
        throw new Base64Error(lastError);
    if (lastError == "BAD_ACCOUNT_KEY")
        throw new AccountKeyError(lastError);
    if (lastError == "UNKNOWN_PICKLE_VERSION" || lastError == "CORRUPTED_PICKLE" || lastError == "BAD_LEGACY_ACCOUNT_PICKLE")
        throw new PickleError(lastError);
    if (lastError == "BAD_SESSION_KEY")
        throw new SessionKeyError(lastError);
    if (lastError == "BAD_SIGNATURE")
        throw new SignatureError(lastError);
    throw new OlmError(lastError);
}
