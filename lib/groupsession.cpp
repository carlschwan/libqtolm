#include "groupsession.h"

#include "errors.h"
#include "utils.h"

#include <QtDebug>

using namespace QtOlm;

InboundGroupSession::InboundGroupSession(QByteArray sessionKey, Initialization type, QObject *parent)
    : QObject(parent)
    , _session(newSession())
{
    if (sessionKey.isEmpty())
        throw new InvalidArgument("Session key is empty");

    if (type == Initialization::Init) {
        checkErr(olm_init_inbound_group_session(_session, reinterpret_cast<const uint8_t *>(sessionKey.data()), sessionKey.length()));
    } else if (type == Initialization::Import) {
        checkErr(olm_import_inbound_group_session(_session, reinterpret_cast<const uint8_t *>(sessionKey.data()), sessionKey.length()));
    }
}

InboundGroupSession::InboundGroupSession(QByteArray sessionKey, QByteArray pickle, QString passphrase, QObject *parent)
    : InboundGroupSession(sessionKey, Initialization::Init, parent)
{
    if (pickle.isEmpty())
        throw new InvalidArgument("Pickle is empty");

    std::string pass = passphrase.toStdString();
    checkErr(olm_unpickle_inbound_group_session(_session, pass.data(), pass.length(), pickle.data(), pickle.length()));
}

InboundGroupSession::~InboundGroupSession()
{
    olm_clear_inbound_group_session(_session);
}

QByteArray InboundGroupSession::pickle(QString passphrase)
{
    std::string pass = passphrase.toStdString();
    size_t pickleLength = olm_pickle_inbound_group_session_length(_session);
    QByteArray pickleBuffer(pickleLength, '0');
    checkErr(olm_pickle_inbound_group_session(_session, pass.data(), pass.length(), pickleBuffer.data(), pickleLength));
    return pickleBuffer;
}

std::pair<QString, uint32_t> InboundGroupSession::decrypt(QByteArray cipherText)
{
    if (cipherText.isEmpty())
        throw new InvalidArgument("Message is empty");

    QByteArray cipherTextBuffer = cipherText;
    size_t maxPlainTextLength = olm_group_decrypt_max_plaintext_length(_session, reinterpret_cast<uint8_t *>(cipherTextBuffer.data()), cipherTextBuffer.length());

    QByteArray plainTextBuffer(maxPlainTextLength, '0');
    cipherTextBuffer = cipherText;

    uint32_t messageIndex;

    size_t plainTextLength = olm_group_decrypt(_session, reinterpret_cast<uint8_t *>(cipherTextBuffer.data()), cipherTextBuffer.length(), reinterpret_cast<uint8_t *>(plainTextBuffer.data()), maxPlainTextLength, &messageIndex);
    checkErr(plainTextLength);
    plainTextBuffer.truncate(plainTextLength);
    return std::pair<QString, uint32_t>(QString(plainTextBuffer), messageIndex);
}

QString InboundGroupSession::id()
{
    size_t idLength = olm_inbound_group_session_id_length(_session);
    QByteArray idBuffer(idLength, '0');
    checkErr(olm_inbound_group_session_id(_session, reinterpret_cast<uint8_t *>(idBuffer.data()), idLength));
    return idBuffer;
}

int InboundGroupSession::firstKnownIndex()
{
    return olm_inbound_group_session_first_known_index(_session);
}

QByteArray InboundGroupSession::exportSession(int messageIndex)
{
    size_t exportLength = olm_export_inbound_group_session_length(_session);
    QByteArray exportBuffer(exportLength, '0');
    checkErr(olm_export_inbound_group_session(_session, reinterpret_cast<uint8_t *>(exportBuffer.data()), exportLength, messageIndex));
    return exportBuffer;
}

OlmInboundGroupSession *InboundGroupSession::newSession()
{
    return olm_inbound_group_session(new uint8_t[olm_inbound_group_session_size()]);
}

void InboundGroupSession::checkErr(size_t code)
{
    if (code != olm_error())
        return;
    std::string lastError = olm_inbound_group_session_last_error(_session);

    if (lastError == "SUCCESS")
        return;
    if (lastError == "NOT_ENOUGH_RANDOM")
        throw new EntropyError(lastError);
    if (lastError == "OUTPUT_BUFFER_TOO_SMALL" || lastError == "OLM_INPUT_BUFFER_TOO_SMALL")
        throw new BufferError(lastError);
    if (lastError == "BAD_MESSAGE_VERSION" || lastError == "BAD_MESSAGE_FORMAT" || lastError == "BAD_MESSAGE_MAC" || lastError == "BAD_MESSAGE_KEY_ID" || lastError == "UNKNOWN_MESSAGE_INDEX")
        throw new MessageError(lastError);
    if (lastError == "INVALID_BASE64")
        throw new Base64Error(lastError);
    if (lastError == "BAD_ACCOUNT_KEY")
        throw new AccountKeyError(lastError);
    if (lastError == "UNKNOWN_PICKLE_VERSION" || lastError == "CORRUPTED_PICKLE" || lastError == "BAD_LEGACY_ACCOUNT_PICKLE")
        throw new PickleError(lastError);
    if (lastError == "BAD_SESSION_KEY")
        throw new SessionKeyError(lastError);
    if (lastError == "BAD_SIGNATURE")
        throw new SignatureError(lastError);
    throw new OlmError(lastError);
}

OutboundGroupSession::OutboundGroupSession(QObject *parent)
    : QObject(parent)
    , _session(newSession())
{
    size_t randomLength = olm_init_outbound_group_session_random_length(_session);
    QByteArray randomBuffer = getRandomData(randomLength);
    checkErr(olm_init_outbound_group_session(_session, reinterpret_cast<uint8_t *>(randomBuffer.data()), randomLength));
}

OutboundGroupSession::OutboundGroupSession(QByteArray pickle, QString passphrase, QObject *parent)
    : OutboundGroupSession(parent)
{
    if (pickle.isEmpty())
        throw new InvalidArgument("Pickle is empty");

    std::string pass = passphrase.toStdString();
    checkErr(olm_unpickle_outbound_group_session(_session, pass.data(), pass.length(), pickle.data(), pickle.length()));
}

OutboundGroupSession::~OutboundGroupSession()
{
    olm_clear_outbound_group_session(_session);
}

QByteArray OutboundGroupSession::pickle(QString passphrase)
{
    std::string pass = passphrase.toStdString();
    size_t pickleLength = olm_pickle_outbound_group_session_length(_session);
    QByteArray pickleBuffer(pickleLength, '0');
    checkErr(olm_pickle_outbound_group_session(_session, pass.data(), pass.length(), pickleBuffer.data(), pickleLength));
    return pickleBuffer;
}

QByteArray OutboundGroupSession::encrypt(QString plainText)
{
    std::string plainTxt = plainText.toStdString();
    size_t messageLength = olm_group_encrypt_message_length(_session, plainTxt.length());
    QByteArray messageBuffer(messageLength, '0');
    checkErr(olm_group_encrypt(_session, reinterpret_cast<const uint8_t *>(plainTxt.data()), plainTxt.length(), reinterpret_cast<uint8_t *>(messageBuffer.data()), messageLength));
    return messageBuffer;
}

QString OutboundGroupSession::id()
{
    size_t idLength = olm_outbound_group_session_id_length(_session);
    QByteArray idBuffer(idLength, '0');
    checkErr(olm_outbound_group_session_id(_session, reinterpret_cast<uint8_t *>(idBuffer.data()), idLength));
    return idBuffer;
}

int OutboundGroupSession::messageIndex()
{
    return olm_outbound_group_session_message_index(_session);
}

QByteArray OutboundGroupSession::sessionKey()
{
    size_t keyLength = olm_outbound_group_session_key_length(_session);
    QByteArray keyBuffer(keyLength, '0');
    checkErr(olm_outbound_group_session_key(_session, reinterpret_cast<uint8_t *>(keyBuffer.data()), keyLength));
    return keyBuffer;
}

OlmOutboundGroupSession *OutboundGroupSession::newSession()
{
    return olm_outbound_group_session(new uint8_t[olm_outbound_group_session_size()]);
}

void OutboundGroupSession::checkErr(size_t code)
{
    if (code != olm_error())
        return;
    std::string lastError = olm_outbound_group_session_last_error(_session);

    if (lastError == "SUCCESS")
        return;
    if (lastError == "NOT_ENOUGH_RANDOM")
        throw new EntropyError(lastError);
    if (lastError == "OUTPUT_BUFFER_TOO_SMALL" || lastError == "OLM_INPUT_BUFFER_TOO_SMALL")
        throw new BufferError(lastError);
    if (lastError == "BAD_MESSAGE_VERSION" || lastError == "BAD_MESSAGE_FORMAT" || lastError == "BAD_MESSAGE_MAC" || lastError == "BAD_MESSAGE_KEY_ID" || lastError == "UNKNOWN_MESSAGE_INDEX")
        throw new MessageError(lastError);
    if (lastError == "INVALID_BASE64")
        throw new Base64Error(lastError);
    if (lastError == "BAD_ACCOUNT_KEY")
        throw new AccountKeyError(lastError);
    if (lastError == "UNKNOWN_PICKLE_VERSION" || lastError == "CORRUPTED_PICKLE" || lastError == "BAD_LEGACY_ACCOUNT_PICKLE")
        throw new PickleError(lastError);
    if (lastError == "BAD_SESSION_KEY")
        throw new SessionKeyError(lastError);
    if (lastError == "BAD_SIGNATURE")
        throw new SignatureError(lastError);
    throw new OlmError(lastError);
}
