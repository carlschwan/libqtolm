#include "session.h"

#include "errors.h"
#include "message.h"
#include "utils.h"

#include <QtDebug>

using namespace QtOlm;

Session::Session(QObject *parent)
    : QObject(parent)
    , _session(newSession())
{
}

Session::Session(QByteArray pickle, QString passphrase, QObject *parent)
    : QObject(parent)
    , _session(newSession())
{
    if (pickle.isEmpty())
        throw new InvalidArgument("Pickle is empty");
    std::string pass = passphrase.toStdString();
    checkErr(olm_unpickle_session(_session, pass.data(), pass.length(), pickle.data(), pickle.length()));
}

Session::~Session()
{
    olm_clear_session(_session);
}

QByteArray Session::pickle(QString passphrase)
{
    std::string pass = passphrase.toStdString();
    size_t pickleLength = olm_pickle_session_length(_session);
    QByteArray pickleBuffer(pickleLength, '0');
    checkErr(olm_pickle_session(_session, pass.data(), pass.length(), pickleBuffer.data(), pickleLength));
    return pickleBuffer;
}

_Message *Session::encrypt(QString plainText)
{
    std::string plainTxt = plainText.toStdString();
    size_t randomLength = olm_encrypt_random_length(_session);
    QByteArray randomBuffer = getRandomData(randomLength);
    size_t messageType = olm_encrypt_message_type(_session);
    checkErr(messageType);
    size_t cipherTextLength = olm_encrypt_message_length(_session, plainTxt.length());
    QByteArray cipherTextBuffer(cipherTextLength, '0');
    checkErr(olm_encrypt(_session, plainTxt.data(), plainTxt.length(), randomBuffer.data(), randomLength, cipherTextBuffer.data(), cipherTextLength));

    if (messageType == OLM_MESSAGE_TYPE_PRE_KEY)
        return new PreKeyMessage(cipherTextBuffer);
    else if (messageType == OLM_MESSAGE_TYPE_MESSAGE)
        return new Message(cipherTextBuffer);
    throw new InvalidArgument("Unknown message type");
}

QString Session::decrypt(_Message *message)
{
    if (message->cipherText().isEmpty())
        throw new InvalidArgument("Message is empty");
    QByteArray cipherTextBuffer = message->cipherText();
    size_t maxPlainTextLength = olm_decrypt_max_plaintext_length(_session, message->messageType(), cipherTextBuffer.data(), cipherTextBuffer.length());
    cipherTextBuffer = message->cipherText(); // Invoking olm_decrypt_max_plaintext_length
                                              // changes cipherTextBuffer's content
    QByteArray plainTextBuffer(maxPlainTextLength, '0');
    size_t plainTextLength = olm_decrypt(_session, message->messageType(), cipherTextBuffer.data(), cipherTextBuffer.length(), plainTextBuffer.data(), maxPlainTextLength);
    checkErr(plainTextLength);
    plainTextBuffer.truncate(plainTextLength);
    return QString(plainTextBuffer);
}

bool Session::matches(PreKeyMessage *message, QString identityKey)
{
    if (message->cipherText().isEmpty())
        throw new InvalidArgument("Cipher text is empty");
    QByteArray messageBuffer = message->cipherText();

    size_t ret;

    if (identityKey.isEmpty()) {
        ret = olm_matches_inbound_session(_session, messageBuffer.data(), messageBuffer.length());
    } else {
        QByteArray identityKeyBuffer(identityKey.toUtf8());
        ret = olm_matches_inbound_session_from(_session, identityKeyBuffer.data(), identityKeyBuffer.length(), messageBuffer.data(), messageBuffer.length());
    }
    checkErr(ret);
    return bool(ret);
}

QString Session::id()
{
    size_t idLength = olm_session_id_length(_session);
    QByteArray idBuffer(idLength, '0');
    checkErr(olm_session_id(_session, idBuffer.data(), idLength));
    return idBuffer;
}

void Session::checkErr(size_t code)
{
    if (code != olm_error())
        return;
    std::string lastError = olm_session_last_error(_session);

    if (lastError == "SUCCESS")
        return;
    if (lastError == "NOT_ENOUGH_RANDOM")
        throw new EntropyError(lastError);
    if (lastError == "OUTPUT_BUFFER_TOO_SMALL" || lastError == "OLM_INPUT_BUFFER_TOO_SMALL")
        throw new BufferError(lastError);
    if (lastError == "BAD_MESSAGE_VERSION" || lastError == "BAD_MESSAGE_FORMAT" || lastError == "BAD_MESSAGE_MAC" || lastError == "BAD_MESSAGE_KEY_ID" || lastError == "UNKNOWN_MESSAGE_INDEX")
        throw new MessageError(lastError);
    if (lastError == "INVALID_BASE64")
        throw new Base64Error(lastError);
    if (lastError == "BAD_ACCOUNT_KEY")
        throw new AccountKeyError(lastError);
    if (lastError == "UNKNOWN_PICKLE_VERSION" || lastError == "CORRUPTED_PICKLE" || lastError == "BAD_LEGACY_ACCOUNT_PICKLE")
        throw new PickleError(lastError);
    if (lastError == "BAD_SESSION_KEY")
        throw new SessionKeyError(lastError);
    if (lastError == "BAD_SIGNATURE")
        throw new SignatureError(lastError);
    throw new OlmError(lastError);
}

OlmSession *Session::newSession()
{
    return olm_session(new uint8_t[olm_session_size()]);
}

InboundSession::InboundSession(Account *account, PreKeyMessage *message, QByteArray identityKey, QObject *parent)
    : Session(parent)
{
    if (message->cipherText().isEmpty())
        throw new InvalidArgument("Ciphertext is empty");
    QByteArray messageBuffer = message->cipherText();
    if (identityKey.isEmpty()) {
        checkErr(olm_create_inbound_session(_session, account->account(), messageBuffer.data(), messageBuffer.length()));
    } else {
        checkErr(olm_create_inbound_session_from(_session, account->account(), identityKey.data(), identityKey.length(), messageBuffer.data(), messageBuffer.length()));
    }
}

OutboundSession::OutboundSession(Account *account, QByteArray identityKey, QByteArray oneTimeKey, QObject *parent)
    : Session(parent)
{
    if (identityKey.isEmpty())
        throw new InvalidArgument("Identity key is empty");
    if (oneTimeKey.isEmpty())
        throw new InvalidArgument("One time key is empty");

    size_t sessionRandomLength = olm_create_outbound_session_random_length(_session);
    QByteArray randomBuffer = getRandomData(sessionRandomLength);

    checkErr(olm_create_outbound_session(_session, account->account(), identityKey.data(), identityKey.length(), oneTimeKey.data(), oneTimeKey.length(), randomBuffer.data(), sessionRandomLength));
}
