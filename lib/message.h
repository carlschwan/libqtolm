#ifndef MESSAGE_H
#define MESSAGE_H

#include "olm/olm.h"

#include <QJsonObject>
#include <QMap>
#include <QObject>

namespace QtOlm
{
class _Message : public QObject
{
    Q_OBJECT
    Q_PROPERTY(size_t messageType READ messageType)
    Q_PROPERTY(QByteArray cipherText READ cipherText)
public:
    explicit _Message(QByteArray cipher, size_t type, QObject *parent = nullptr);

    size_t messageType()
    {
        return _messageType;
    }
    QByteArray cipherText()
    {
        return _cipherText;
    }

private:
    size_t _messageType;
    QByteArray _cipherText;
};
class Message : public _Message
{
    Q_OBJECT
public:
    explicit Message(QByteArray cipher, QObject *parent = nullptr)
        : _Message(cipher, OLM_MESSAGE_TYPE_MESSAGE, parent)
    {
    }
};
class PreKeyMessage : public _Message
{
    Q_OBJECT
public:
    explicit PreKeyMessage(QByteArray cipher, QObject *parent = nullptr)
        : _Message(cipher, OLM_MESSAGE_TYPE_PRE_KEY, parent)
    {
    }
};
} // namespace QtOlm

#endif // MESSAGE_H
