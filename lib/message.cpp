#include "message.h"

#include "errors.h"

using namespace QtOlm;

_Message::_Message(QByteArray cipher, size_t type, QObject *parent)
    : QObject(parent)
    , _messageType(type)
    , _cipherText(cipher)
{
    if (cipher.isEmpty())
        throw new InvalidArgument("Ciphertext is empty");
}
