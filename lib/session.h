#ifndef SESSION_H
#define SESSION_H

#include "account.h"
#include "message.h"

#include "olm/olm.h"

#include <QObject>

namespace QtOlm
{
class Session : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id)
public:
    explicit Session(QObject *parent = nullptr);
    Session(QByteArray pickle, QString passphrase = "", QObject *parent = nullptr);
    ~Session();

    QByteArray pickle(QString passphrase = "");

    _Message *encrypt(QString plainText);
    QString decrypt(_Message *message);

    bool matches(PreKeyMessage *message, QString identityKey = "");

    QString id();
    OlmSession *session()
    {
        return _session;
    }

private:
    static OlmSession *newSession();

protected:
    void checkErr(size_t code);

    OlmSession *_session;
};

class InboundSession : public Session
{
    Q_OBJECT
public:
    explicit InboundSession(Account *account, PreKeyMessage *message, QByteArray identityKey = "", QObject *parent = nullptr);
};

class OutboundSession : public Session
{
    Q_OBJECT
public:
    explicit OutboundSession(Account *account, QByteArray identityKey, QByteArray oneTimeKey, QObject *parent = nullptr);
};
} // namespace QtOlm

#endif // SESSION_H
