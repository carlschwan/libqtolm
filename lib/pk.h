#ifndef PK_H
#define PK_H

#include <QObject>

#include "olm/olm.h"
#include "olm/pk.h"

namespace QtOlm
{
class PkMessage : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QByteArray ephermalKey READ ephermalKey)
    Q_PROPERTY(QByteArray mac READ mac)
    Q_PROPERTY(QByteArray cipherText READ cipherText)
public:
    explicit PkMessage(QByteArray ephermalKey, QByteArray mac, QByteArray cipherText, QObject *parent = nullptr);
    virtual ~PkMessage()
    {
    }

    QByteArray ephermalKey()
    {
        return _ephermalKey;
    }
    QByteArray mac()
    {
        return _mac;
    }
    QByteArray cipherText()
    {
        return _cipherText;
    }

private:
    QByteArray _ephermalKey;
    QByteArray _mac;
    QByteArray _cipherText;
};

class PkEncryption : public QObject
{
    Q_OBJECT
public:
    explicit PkEncryption(QByteArray recipientKey, QObject *parent = nullptr);
    ~PkEncryption();

    PkMessage *encrypt(QString plainText);

private:
    static OlmPkEncryption *newPkEncryption();
    void checkErr(size_t code);

    OlmPkEncryption *_pkEncryption;
};

class PkDecryption : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QByteArray publicKey READ publicKey)
public:
    explicit PkDecryption(QObject *parent = nullptr);
    PkDecryption(QByteArray pickle, QString passphrase = "", QObject *parent = nullptr);
    ~PkDecryption();

    QByteArray pickle(QString passphrase = "");

    QString decrypt(PkMessage *message);

    QByteArray publicKey()
    {
        return _publicKey;
    }

private:
    static OlmPkDecryption *newPkDecryption();
    void checkErr(size_t code);

    OlmPkDecryption *_pkDecryption;
    QByteArray _publicKey;
};
} // namespace QtOlm

#endif // PK_H
