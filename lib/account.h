#ifndef ACCOUNT_H
#define ACCOUNT_H

#include "olm/olm.h"

#include <QObject>

namespace QtOlm
{
class Session;
class Account : public QObject
{
    Q_OBJECT
public:
    explicit Account(QObject *parent = nullptr);
    Account(QByteArray pickle, QString passphrase = "", QObject *parent = nullptr);
    ~Account();

    QByteArray pickle(QString passphrase = "");

    QPair<QByteArray, QByteArray> identityKeys();
    QByteArray curve25519IdentityKey();
    QByteArray ed25519IdentityKey();

    QByteArray sign(QString message);
    QByteArray sign(QJsonObject message);

    int maxOneTimeKeys();
    void markKeysAsPublished();

    void generateOneTimeKeys(int count);

    QJsonObject oneTimeKeys();
    QVariantHash curve25519OneTimeKeys();
    QVariantHash ed25519OneTimeKeys();
    void removeOneTimeKeys(Session *session);

    OlmAccount *account()
    {
        return _account;
    }

private:
    static OlmAccount *newAccount();

protected:
    OlmAccount *_account;

    void checkErr(size_t code);
};
} // namespace QtOlm

#endif // ACCOUNT_H
